package com.fortumotestexercise;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IntegerHolder {

  private final List<BigInteger> integers = Collections.synchronizedList(new ArrayList<>());
  private volatile BigInteger sum = BigInteger.ZERO;
  private volatile boolean endReceived = false;

  public void add(BigInteger integer) {
    if (!endReceived) {
      integers.add(integer);
    }
  }

  public void sumAndEnd() {
    sum = integers.stream().reduce(BigInteger.ZERO, BigInteger::add);
    endReceived = true;
  }

  public void reset() {
    endReceived = false;
    sum = BigInteger.ZERO;
    integers.clear();
  }

  public boolean isEndReceived() {
    return endReceived;
  }

  public BigInteger getSum() {
    return sum;
  }

}
