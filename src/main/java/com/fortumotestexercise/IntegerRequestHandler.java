package com.fortumotestexercise;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IntegerRequestHandler {

  private static final int INITIAL_CAPACITY = 100;
  private final List<Thread> threads = Collections.synchronizedList(new ArrayList<>(INITIAL_CAPACITY));
  private final IntegerHolder integerHolder = new IntegerHolder();

  public BigInteger handleTerminatingRequest() {
    integerHolder.sumAndEnd();
    BigInteger sum = integerHolder.getSum();

    while (!threads.isEmpty()) {
      Thread.yield();
    }

    integerHolder.reset();
    return sum;
  }

  public BigInteger handleIntegerRequest(Long integer) {
    threads.add(Thread.currentThread());
    integerHolder.add(BigInteger.valueOf(integer));

    while (!integerHolder.isEndReceived()) {
      Thread.yield();
    }

    BigInteger sum = integerHolder.getSum();
    threads.remove(Thread.currentThread());
    return sum;
  }

}
