package com.fortumotestexercise;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class IncrementerServer {

  private static final int PORT = 8080;

  public static void main(String[] args) throws Exception {
    Tomcat tomcat = new Tomcat();
    tomcat.setPort(PORT);
    tomcat.getConnector();

    Context context = tomcat.addContext("", new File(".").getAbsolutePath());

    Tomcat.addServlet(context, "EmbeddedIntegerServlet", new HttpIntegerServlet());

    context.addServletMappingDecoded("/*", "EmbeddedIntegerServlet");
    tomcat.start();
    tomcat.getServer().await();
  }
}
