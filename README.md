# Incrementer server
A small HTTP application that accepts POST requests with integers 
and returns their sum once an appropriate terminating keyword has been received.

## Running the application

### Using the embedded Tomcat server
From the command line:
```
./gradlew run
```

### Using a standalone Tomcat installation
Export the servlet as a WAR:
```
./gradlew war
```

Place the archive in Tomcat's `/webapps` directory. The archive should be deployed automatically if the server is 
already running or once it is started.

## Making requests
POST requests can be made to `localhost:8080` in the embedded server and to `localhost:8080/fortumotest/integers` in 
the standalone server. By default, the embedded server should be able to accept at least 100 concurrent connections 
without needing additional configuration. When using the standalone server, however, enabling the non-blocking I/O 
mode is recommended. Simply edit Tomcat's `conf/server.xml` and change the `protocol` property of the server's 
connector to `org.apache.coyote.http11.Http11NioProtocol`.

An example `server.xml` configuration which enables NIO:
```
<Connector
    port="8080"
    protocol="org.apache.coyote.http11.Http11NioProtocol"
    connectionTimeout="20000"
    redirectPort="8443" />
```

### Making requests with `curl`
Adding integers. This request will not receive a response until a terminating keyword has been received in a 
separate request.
```
curl -d 10 http://localhost:8080/
// localhost:8080/fortumotest/integers
```

Sending a terminating keyword ("end" by default). This will cause the application to sum up the numbers received so far
and return the result to each request. The application will then reset its internal state and start accepting 
new numbers.

```
curl -d end http://localhost:8080 
// localhost:8080/fortumotest/integers
```